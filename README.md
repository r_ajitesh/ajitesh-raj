# README #

Welcome to the Cyberdesign Works code example repository.

### What is this repository for? ###

Use this repository to submit your code examples by following the steps below.

Wondering what to submit? Please focus on PHP code (snippets of views are ok too). [There's good advice here :-)](http://programmers.stackexchange.com/questions/51148/what-should-my-code-sample-look-like)

### Submission? ###

* Create a fork of this repository, giving the fork your full name.
* Commit your example code on your fork.
* Create a pull request to the master branch of this repository.
* Done.